package spreadsheetUpdates.observer;

import java.util.ArrayList;
import java.util.List;

public class Cell implements SubjectInterface, ObserverInterface{
	private String name;
	private Integer value;

	private List<Cell> observers;//dependents
	
	public Cell(String name_in){
		name = name_in;
		observers = new ArrayList<Cell>();
		value = 0;
	}
	
	public String getName() {
		return name;
	}
	
	public Integer getValue(){
		return value;
	}
	
	public void setValue(Integer value_in) {
		value += value_in;
	}
	
	public List<Cell> getObservers() {
		return observers;
	}

	public void setObservers(List<Cell> observers) {
		this.observers = observers;
	}

	@Override
	public void update(Integer value_in) {
		// TODO Auto-generated method stub
		value+=value_in;
	}

	@Override
	public void registerObserver(Cell o) {
		// TODO Auto-generated method stub
		observers.add((Cell) o);
	}

	@Override
	public void removeObserver(Cell o) {
		// TODO Auto-generated method stub
		observers.remove(o);
	}

	@Override
	public void notifyObservers() {
		// TODO Auto-generated method stub
		
	}

	public String toString(){
		return this.getName()+" "+this.getValue();
	}
}
