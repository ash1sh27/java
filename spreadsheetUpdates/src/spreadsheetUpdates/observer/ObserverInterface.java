package spreadsheetUpdates.observer;

public interface ObserverInterface {
	public void update(Integer value);
}
