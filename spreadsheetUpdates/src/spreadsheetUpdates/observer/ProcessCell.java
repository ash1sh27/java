package spreadsheetUpdates.observer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import spreadsheetUpdates.store.Results;
import spreadsheetUpdates.util.FileProcessorInterface;

public class ProcessCell {
	private FileProcessorInterface fp;
	private Results r;
	private String line = null;
	
	public ProcessCell(FileProcessorInterface fpi, Results rin) {
		fp = fpi;
		r = rin;
		initializeMap(fp, r);
	}
	
	private void initializeMap(FileProcessorInterface fp2, Results r2) {
		String[] parts = null;
		Cell new_observer = null,subject = null, old_observer = null;
		while (null != (line=fp2.readLineFromFile())) {
			parts = line.split("=");
			if(r2.getCell_map().containsKey(parts[0]))
				old_observer = r2.getCell_map().get(parts[0]);
			else {
				new_observer = new Cell(parts[0]);
				r2.getCell_map().put(parts[0], new_observer);
			}
				
			String[] subjects = parts[1].split("\\+");
			for(String str: subjects) {
				if(isNum(str)) {
					if(null != old_observer)
						old_observer.setValue(Integer.parseInt(str));
					else 
						new_observer.setValue(Integer.parseInt(str));
				}
				else {
					if(r2.getCell_map().containsKey(str))
						subject = r2.getCell_map().get(str);
					else {
						subject = new Cell(str);
						r2.getCell_map().put(str, subject);
					}
						
					if(null != old_observer){
						subject.registerObserver(old_observer);
						old_observer.update(subject.getValue());
					}
					else {
						subject.registerObserver(new_observer);
						new_observer.update(subject.getValue());
					}
				}
			}
			
		
		/*Set<Cell> existing_cells = new HashSet<Cell>();
		if(detectCycle(new_observer, existing_cells)) {
			System.out.println("HERE");
			System.out.println("Cycle detected");
			r2.cell_map.put(parts[0], old_observer);
		}
		else
			r2.cell_map.put(parts[0], new_observer);
		}*/
		/*Set<String> keys = r2.cell_map.keySet();
		System.out.println();
        for(String key: keys)
            System.out.println(r2.cell_map.get(key));*/
		}
	}
	
	private boolean isNum(String s) {
		boolean result = false;
		try{
			Integer.parseInt(s);
			result = true;
		}
		catch(NumberFormatException e) {
			//e.printStackTrace();
			//System.exit(1);
		}
		finally {
			
		}
		return result;
	}
	
	private boolean detectCycle(Cell cell, Set<Cell> existing_cells){
		System.out.println("detectCycle called");
		if(existing_cells.contains(cell))
			return true;
		existing_cells.add(cell);
		/*for (Cell cel :existing_cells)
			System.out.println(cell);*/
		ArrayList<Cell> observers = (ArrayList<Cell>) cell.getObservers();
		
		for(Cell c : observers) {
			if(null != c)
				if(detectCycle(c, existing_cells))
					return true;
		}
		existing_cells.remove(cell);
		return false;
	}
}