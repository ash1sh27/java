package spreadsheetUpdates.util;

public interface FileProcessorInterface {
	public String readLineFromFile();
	public void writeLineToFile(String line);
}