
package spreadsheetUpdates.driver;

import java.io.File;

//import spreadsheetUpdates.observer.Cell;
import spreadsheetUpdates.observer.ProcessCell;
import spreadsheetUpdates.store.Results;
import spreadsheetUpdates.util.FileProcessor;
import spreadsheetUpdates.util.FileProcessorInterface;
import spreadsheetUpdates.util.Logger;
import spreadsheetUpdates.util.Logger.DebugLevel;

public class Driver{

	
	public static void main(String args[]) {
		System.out.println(args[0]);
		System.out.println(args[1]);
		System.out.println(args[2]);
	    Driver dr = new Driver();
	    dr.validateArgs(args);

	    System.out.println("\n Geting Started\n");
	    
	    FileProcessorInterface fpi = new FileProcessor(args[0],args[1]);
	    Results r = new Results(fpi);
	    ProcessCell pc = new ProcessCell(fpi, r);
	    r.writeSumToStdout();
	    r.writeSumToFile();
	} // end main(...)
	
	private void validateArgs(String args[]){
		//validate number of arguments
		if(args.length==3){
		    // get file names
			
			try{
				String inputfile = args[0];
				File file;
				file = new File(inputfile);
				if(!file.exists() || !file.canRead() || null==inputfile){
					System.err.println("The input file does not exist or is null.");
					System.exit(1);
				}
				String outputfile = args[1];
				if(null==outputfile){
					System.err.println("Please provide a valid .txt filename as argument.");
					System.exit(1);
				}
				Integer debuglevel = Integer.parseInt(args[2]);
				Logger.setDebugValue(debuglevel); 

			}catch(IllegalArgumentException ex){
				System.err.println("NumberFormatException-Cannot parse to integer.");
				ex.printStackTrace();
				System.exit(1);
			}
			catch(Exception ex){
				System.err.println("Exception occured.");
				ex.printStackTrace();
				System.exit(1);
			}
			finally{

			}
		}else{
			System.err.println("Invalid number of arguments. Expected [FIXME: provide details here]");
			System.exit(1);
		}
	}

    

} // end public class Driver

