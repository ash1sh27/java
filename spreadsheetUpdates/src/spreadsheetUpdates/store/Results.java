
package spreadsheetUpdates.store;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import spreadsheetUpdates.observer.Cell;
import spreadsheetUpdates.util.FileDisplayInterface;
import spreadsheetUpdates.util.FileProcessorInterface;
import spreadsheetUpdates.util.Logger;
import spreadsheetUpdates.util.Logger.DebugLevel;
import spreadsheetUpdates.util.StdoutDisplayInterface;

public class Results implements StdoutDisplayInterface, FileDisplayInterface {

    // private data structure
	private Map<String, Cell> cell_map;
	private FileProcessorInterface fpi;
	/**
	 * Constructor
	 */
	public Results(FileProcessorInterface fpi_in) {
		Logger.writeMessage("Results Constructor is called ", DebugLevel.CONSTRUCTOR);
		cell_map = new HashMap<String, Cell>();
		fpi = fpi_in;
	}
    // methods to read/write from/to the data structure

	public Map<String, Cell> getCell_map() {
		return cell_map;
	}

	public void setCell_map(Map<String, Cell> cell_map) {
		this.cell_map = cell_map;
	}	
    
	/**
	 * 
	 * @param map2 (Map interface variable)
	 * @return double
	 */
	public int sumOfCells(Map<String, Cell> cell_map_in) {
		int sum=0;
		
		for(Cell c: cell_map_in.values()){
    		sum += c.getValue();
    	}
		return sum;	
	}
	
	@Override
	public void writeSumToStdout() {
		// TODO Auto-generated method stub
		System.out.println("The sum of all cell values is: "+ sumOfCells(cell_map));
	}
	
    /**
     * Writes the results to the output file.
     */
    @Override
    public void writeSumToFile(){
    	fpi.writeLineToFile("The sum of all cell values is: "+ sumOfCells(cell_map));
    }
	
	/**
	 * Display the ConcurrentLinkedHashMap(unsorted) of results and the 
	 * TreeMap(sorted) of the results.
	 */
	@Override
	public String toString() {
		return "Map"+cell_map;
	}

    // other methods
}